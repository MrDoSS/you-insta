import { i18n } from 'boot/i18n'
import validation from 'boot/validation'

export default {
  id: 'text',
  label: i18n.t('widgets.text'),
  icon: 'receipt',
  color: 'primary',
  fields: {
    text: {
      label: i18n.t('widgets.text'),
      component: 'q-input',
      type: 'textarea',
      required: 'true',
      rules: [validation.required],
      default: i18n.t('widgets.default.text.text'),
      outlined: true
    }
  }
}
