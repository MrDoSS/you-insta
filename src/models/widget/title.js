import { i18n } from 'boot/i18n'
import validation from 'boot/validation'

export default {
  id: 'title',
  label: i18n.t('widgets.title'),
  icon: 'title',
  color: 'secondary',
  fields: {
    title: {
      label: i18n.t('widgets.title'),
      component: 'q-input',
      type: 'text',
      rules: [validation.required],
      default: i18n.t('widgets.default.title.title')
    }
  }
}
