export default {
  namespaced: true,
  state: {
    activeTab: 'builder'
  },
  mutations: {
    SET_ACTIVE_TAB (state, value) {
      state.activeTab = value
    }
  },
  actions: {
    async bindAll ({ dispatch }, siteId) {
      await dispatch('db/widgets/bind', siteId, { root: true })
    },
    unbindAll ({ dispatch }) {
      dispatch('db/widgets/unbind', null, { root: true })
    }
  }
}
