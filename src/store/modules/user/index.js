import { storage } from 'boot/firebase'

export default {
  state: {
    loggedIn: false,
    data: {}
  },
  mutations: {
    SET_LOGGED_IN (state, value) {
      state.loggedIn = value
    },
    SET_USER (state, data) {
      state.data = data
    },
    SET_AVATAR_URL (state, url) {
      state.data.avatarUrl = url
    }
  },
  getters: {
    isAuth (state) {
      return state.loggedIn
    }
  },
  actions: {
    async fetchUser ({ commit, dispatch }, user) {
      commit('SET_LOGGED_IN', user !== null)
      if (user) {
        commit('SET_USER', {
          uid: user.uid,
          displayName: user.displayName,
          email: user.email
        })
        dispatch('fetchAvatarUrl', user.photoURL)
      } else {
        commit('SET_USER', {})
      }
    },
    async fetchAvatarUrl ({ commit }, gsUrl) {
      try {
        const gsReference = storage.refFromURL(gsUrl)
        const url = await gsReference.getDownloadURL()
        commit('SET_AVATAR_URL', url)
      } catch (e) {
        return null
      }
    }
  }
}
