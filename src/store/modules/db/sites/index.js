import { firestoreAction } from 'vuexfire'
import { firebase, db } from 'boot/firebase'

export default {
  namespaced: true,
  state: {
    items: [],
    bound: false
  },
  mutations: {
    SET_BOUND (state, value) {
      state.bound = value
    }
  },
  actions: {
    bind: firestoreAction(async ({ bindFirestoreRef, commit, rootState }) => {
      const userId = rootState.user.data.uid
      const ref = db.collection('sites')
        .where('userId', '==', userId)
        .orderBy('createdAt')

      return bindFirestoreRef('items', ref)
        .then(commit('SET_BOUND', true))
    }),
    unbind: firestoreAction(({ unbindFirestoreRef, commit }) => {
      unbindFirestoreRef('items')
      commit('SET_BOUND', false)
    }),
    add: firestoreAction(({ state, rootState }, data) => {
      const userId = rootState.user.data.uid
      return db.collection('sites').add({
        userId: userId,
        ...data,
        createdAt: firebase.firestore.Timestamp.now()
      })
    }),
    update: firestoreAction(({ state }, data) => {
      const { id, ...payload } = data
      return db.collection('sites')
        .doc(id)
        .update(payload)
    }),
    updateAll: firestoreAction((_context, items) => {
      const batch = db.batch()
      for (const item of items) {
        const { id, ...payload } = item
        batch.update(db.collection('sites').doc(id), payload)
      }
      return batch.commit()
    }),
    delete: firestoreAction((_context, id) => {
      return db.collection('sites').doc(id).delete()
    }),
    checkUrl (_, url) {
      return db.collection('sites').where('url', '==', url).get()
    }
  }
}
