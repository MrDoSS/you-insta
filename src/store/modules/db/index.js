import widgets from './widgets'
import sites from './sites'

export default {
  namespaced: true,
  modules: { widgets, sites }
}
