import { firestoreAction } from 'vuexfire'
import { firebase, db } from 'boot/firebase'
import mapDataToCreate from 'src/services/widgets/mapDataToCreate'

export default {
  namespaced: true,
  state: {
    items: [],
    bound: false,
    siteId: null,
    ref: null
  },
  mutations: {
    SET_BOUND (state, value) {
      state.bound = value
    },
    SET_SITE_ID (state, value) {
      state.siteId = value
    },
    SET_REF_PATH (state, value) {
      state.ref = value
    }
  },
  actions: {
    bind: firestoreAction(async ({ bindFirestoreRef, commit }, siteId) => {
      const ref = db.collection(`sites/${siteId}/widgets`)
      return bindFirestoreRef('items', ref.orderBy('order'))
        .then(() => {
          commit('SET_BOUND', true)
          commit('SET_SITE_ID', siteId)
          commit('SET_REF_PATH', ref.path)
        })
    }),
    unbind: firestoreAction(({ unbindFirestoreRef, commit }) => {
      unbindFirestoreRef('items')
      commit('SET_BOUND', false)
      commit('SET_SITE_ID', null)
      commit('SET_REF_PATH', null)
    }),
    add: firestoreAction(({ state }, data) => {
      return db.collection(state.ref).add({
        ...mapDataToCreate(data),
        order: state.items.length + 1,
        createdAt: firebase.firestore.Timestamp.now()
      })
    }),
    update: firestoreAction(({ state }, data) => {
      const { id, ...payload } = data
      return db.collection(state.ref).doc(id).update(payload)
    }),
    updateAll: firestoreAction(({ state }, items) => {
      const batch = db.batch()
      for (const item of items) {
        const { id, ...payload } = item
        batch.update(db.collection(state.ref).doc(id), payload)
      }
      return batch.commit()
    }),
    delete: firestoreAction(({ state }, id) => {
      return db.collection(state.ref).doc(id).delete()
    })
  }
}
