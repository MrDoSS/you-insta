import Vue from 'vue'
import Vuex from 'vuex'

import db from './modules/db'
import user from './modules/user'
import site from './modules/site'
import { vuexfireMutations } from 'vuexfire'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      db,
      user,
      site
    },
    mutations: {
      ...vuexfireMutations
    },

    strict: process.env.DEV
  })

  return Store
}
