const withPrefix = (prefix, routes) =>
  routes.map((route) => {
    route.path = `${prefix}/${route.path}`
    return route
  })

const routes = [
  {
    path: '/dashboard',
    component: () => import('layouts/Dashboard.vue' /* webpackChunkName: "dashboard-layout" */),
    children: [
      {
        path: '',
        name: 'dashboard',
        component: () => import('pages/dashboard/Index.vue' /* webpackChunkName: "dashboard-ibdex" */),
        meta: {
          hideDraver: true,
          primaryHeader: true,
          showHeader: true
        }
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import('pages/Profile.vue' /* webpackChunkName: "profile" */),
        meta: {
          hideDraver: true,
          primaryHeader: true,
          showHeader: true
        }
      },
      ...withPrefix('sites', [
        {
          path: 'new',
          name: 'dashboard-sites-new',
          component: () => import('pages/dashboard/sites/New.vue' /* webpackChunkName: "dashboard-sites-new" */),
          meta: {
            hideDraver: true,
            primaryHeader: true,
            showHeader: true
          }
        },
        {
          path: ':id',
          name: 'dashboard-sites-show',
          component: () => import('pages/dashboard/sites/Show.vue' /* webpackChunkName: "dashboard-sites-show" */)
        }
      ])
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    component: () => import('layouts/Common.vue' /* webpackChunkName: "common-layout" */),
    children: [{
      path: '',
      name: 'index',
      component: () => import('pages/Index.vue' /* webpackChunkName: "index" */)
    }, {
      path: 'login',
      name: 'login',
      component: () => import('pages/Login.vue' /* webpackChunkName: "login" */)
    }, {
      path: 'registration',
      name: 'registration',
      component: () => import('pages/Registration.vue' /* webpackChunkName: "registration" */)
    }],
    meta: {
      requiresAuth: false
    }
  }
]

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/errors/404.vue' /* webpackChunkName: "404" */)
  })
}

export default routes
