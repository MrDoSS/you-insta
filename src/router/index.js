import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function ({ Vue, store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      Vue.prototype.$auth.onAuthStateChanged(user => {
        if (!user) {
          next({
            name: 'login',
            query: {
              redirect: to.name
            }
          })
        } else {
          next()
        }
      })
    } else if (to.matched.some(record => record.meta.requiresAuth === false)) {
      Vue.prototype.$auth.onAuthStateChanged(user => {
        if (user) {
          next({ name: 'dashboard' })
        } else {
          next()
        }
      })
    } else {
      next()
    }
  })

  return Router
}
