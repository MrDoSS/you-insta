import isEmpty from 'lodash/isEmpty'
import isPlainObject from 'lodash/isPlainObject'
import lodashKeys from 'lodash/keys'
import isArray from 'lodash/isArray'
import every from 'lodash/every'
import map from 'lodash/map'

export default function isDeepEmpty (obj) {
  if (!obj) return true

  if (isEmpty(obj)) return true

  if (isPlainObject(obj)) {
    if (lodashKeys(obj).length === 0) return true
    return every(map(obj, v => isDeepEmpty(v)))
  } else if (isArray(obj)) {
    if (obj.length === 0) return true
    return every(obj, v => isDeepEmpty(v))
  }
  return false
}
