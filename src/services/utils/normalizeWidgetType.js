import kebabCase from 'lodash/kebabCase'

export default function (type) {
  return kebabCase(`${type}-widget`)
}
