import { Loading } from 'quasar'

export default async function (fn, ...args) {
  try {
    Loading.show()
    return await fn(...args)
  } catch (e) {
    throw e
  } finally {
    Loading.hide()
  }
}
