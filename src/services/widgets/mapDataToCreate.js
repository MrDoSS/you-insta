export default function (data) {
  const mappedData = {
    type: data.id
  }

  for (const field in data.fields) {
    mappedData[field] = data.fields[field].default
  }

  return mappedData
}
