import auth from './auth'
import validation from './validation'
import widgets from './widgets'

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  essentialLinks: 'Essential Links',
  dashboard: 'Dashboard',
  site: 'Site',
  builder: 'Builder',
  settings: 'Settings',
  or: 'Or',
  availableWidgets: 'Available widgets',
  close: 'Close',
  save: 'Save',
  edit: 'Edit',
  remove: 'Remove',
  editWidget: 'Edit widget',
  yourSites: 'Your sites',
  addSite: 'Add site',
  newSite: 'New site',
  siteAddress: 'Website address',
  siteTitle: 'Website title',
  permissionDenied: 'Permission denied',
  deleteSiteWarning: 'You are about to delete the site «{siteTitle}».',
  confirm: 'Confirm',
  somethingError: 'Something went wrong',
  theme: 'Theme',
  profile: 'Profile',
  signOut: 'Sign out',
  auth,
  validation,
  widgets
}
