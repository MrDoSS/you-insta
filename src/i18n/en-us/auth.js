export default {
  signIn: 'Sign in',
  signUp: 'Sign up',
  username: 'Username',
  email: 'Email',
  password: 'Password',
  notMember: 'Not a member yet?',
  haveAccount: 'Already have an account?',
  errors: {
    'auth/user-not-found': 'There is no user record corresponding to this identifier. The user may have been deleted.',
    'auth/wrong-password': 'The password is invalid or the user does not have a password.',
    'auth/email-already-in-use': 'The email address is already in use by another account.'
  }
}
