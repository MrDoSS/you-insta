export default {
  required: 'Field is required',
  email: 'The email address is badly formatted.',
  password: 'The password must be 6 characters long or more.',
  url: 'The site address is badly formatted.',
  urlExists: 'This address is already in use'
}
