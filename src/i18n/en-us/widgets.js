export default {
  text: 'Text',
  title: 'Title',
  default: {
    text: {
      text: 'Some text'
    },
    title: {
      title: 'Some title'
    }
  }
}
