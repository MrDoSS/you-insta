import Vue from 'vue'
import isEmpty from 'lodash/isEmpty'
import { i18n } from 'boot/i18n'

const validation = {
  required: val => !isEmpty(val) || i18n.t('validation.required'),
  email: val => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(val.toLowerCase()) || i18n.t('validation.email')
  },
  password: val => val.length >= 6 || i18n.t('validation.password'),
  url: val => /^[a-zA-Z0-9-]*$/.test(val) || i18n.t('validation.url')
}

Vue.prototype.$validation = validation

export default validation
