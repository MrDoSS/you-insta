import Vue from 'vue'
import { normalizeWidgetType } from 'src/services/utils'

const requireWidgets = require.context('components/widgets', true, /\.(vue)$/, 'lazy')

for (let fileName of requireWidgets.keys()) {
  fileName = fileName.split('/').pop().replace(/\.\w+$/, '')

  Vue.component(
    normalizeWidgetType(fileName),
    () => import(`components/widgets/${fileName}` /* webpackChunkName: "widget" */)
  )
}
