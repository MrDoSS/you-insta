import Vue from 'vue'
import * as firebase from 'firebase/app'
import firebaseConfig from '~/firebase.config'

import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

Vue.prototype.$firebase = firebase
Vue.prototype.$db = db
Vue.prototype.$auth = auth
Vue.prototype.$storage = storage

export default ({ store }) => {
  auth.onAuthStateChanged(user => {
    store.dispatch('fetchUser', user)
  })
}

export { firebase, auth, db, storage }
