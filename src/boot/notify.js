import Vue from 'vue'
import { Notify } from 'quasar'

const notifyError = (message) => {
  Notify.create({
    color: 'negative',
    textColor: 'white',
    position: 'top',
    message,
    timeout: 2000
  })
}

Vue.prototype.$notify = {
  error: notifyError
}

export { notifyError }
