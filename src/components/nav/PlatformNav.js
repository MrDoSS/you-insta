import { Platform } from 'quasar'

const nav = Platform.is.mobile ? 'Mobile' : 'Desktop'
const Component = () => import(`components/nav/${nav}` /* webpackChunkName: "platform-nav" */)

export default {
  functional: true,
  render (createElement) {
    return createElement(Component)
  }
}
